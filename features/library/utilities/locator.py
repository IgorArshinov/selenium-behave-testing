from collections import namedtuple

LocatorTuple = namedtuple('Locator', ['by', 'value'])
