# Selenium-Behave-Testing

Five automated tests for the 5 test scenarios that are programmed with Selenium and Behave. 

The Test Scenario Details.docx file in the documents directory  contains more details about the test scenarios.  

1. Setup
    * Install Google Chrome 80.0.x and ChromeDriver 8.0;
    * Clone the repository;
	* Install the dependencies `pip install -r requirements.txt`.
	
2. Run
    * In a terminal navigate to the features directory;
    * Run the tests with the command `behave`.
