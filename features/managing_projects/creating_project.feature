Feature: Creating project

  Scenario Outline: Validate Next-gen Project web form with valid input

  As an Agile team member, I can create my Next-gen project, so that my team can start working on our project.

    Given I navigate to "Atlassian Home Page"
    And I click on link Login
    And I enter <User> credentials
    And I click on button Inloggen
    And I click on Jira Software
    When I click on button Project aanmaken to create <Project>
    And I click on button Next-gen selecteren
    And I create Next-gen project using valid values
    And I click on button Maken
    Then I validate that Next-gen project is created

    Examples:
      | User            | Project                                   |
      | "Igor Arshinov" | "Valid Next-gen private Kanban project"   |
      | "Igor Arshinov" | "Valid Next-gen public Scrum project"     |
      | "Igor Arshinov" | "Valid Next-gen restricted Scrum project" |


  Scenario Outline: Validate Next-gen Project details are visible on Projects page

  As an Agile team member, I can see my Next-gen project’s Name, Key, Type and Lead, so that I clearly can see my project’s details.

    Given I navigate to "Atlassian Home Page"
    And I click on link Login
    And I enter <User> credentials
    And I click on button Inloggen
    And I click on Jira Software
    And I click on button Project aanmaken to create <Project>
    And I click on button Next-gen selecteren
    And I create Next-gen project using valid values
    And I click on button Maken
    When I click on link Projecten
    Then I can see details of created Next-gen project

    Examples:
      | User            | Project                                   |
      | "Igor Arshinov" | "Valid Next-gen private Kanban project"   |
      | "Igor Arshinov" | "Valid Next-gen public Scrum project"     |
      | "Igor Arshinov" | "Valid Next-gen restricted Scrum project" |





