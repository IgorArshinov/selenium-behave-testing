from library.data import Project, ProjectItem
from library.elements import BaseElement
from library.pages import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from library.utilities import InputHelper
from library.utilities import LocatorTuple


class DialogElement(BaseElement, BasePage):
    current_project = Project()

    def __init__(self, context):
        BaseElement.__init__(
            self,
            context.browser,
            LocatorTuple(by=By.CSS_SELECTOR,
                         value="div[role='dialog']"),
        )
        self._add_project_shortcut_item_span = BaseElement()
        self._project_shortcut_item_name_input = BaseElement()
        self._project_item_type_header_h4 = BaseElement()
        self._add_item_dialog_div = BaseElement()
        self._add_shortcut_item_dialog_div = BaseElement()
        self._project_item_web_address_input = BaseElement()

    @property
    def add_shortcut_item_dialog_div(self) -> BaseElement:
        return self.get_base_element_child(self._add_shortcut_item_dialog_div, self,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//h1[contains(text(),'Snelkoppeling toevoegen')]/ancestor::div[@role='dialog']"))

    @property
    def add_item_dialog_div(self) -> BaseElement:
        return self.get_base_element_child(self._add_item_dialog_div, self,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//h4 //span[contains(@class, 'css-1ezc8jo') and contains(text(),'Voeg een item toe aan dit project')]/ancestor::div[@role='dialog']"))

    @property
    def project_shortcut_item_web_address_input(self) -> BaseElement:
        return self.get_base_element_child(self._project_item_web_address_input, self.add_shortcut_item_dialog_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//span[contains(text(),'Webadres')]/ancestor::div[contains(@class, 'FieldTextStateless__Wrapper-ynbdsh-0')] //input"))

    @property
    def project_shortcut_item_name_input(self) -> BaseElement:
        return self.get_base_element_child(self._project_shortcut_item_name_input, self.add_shortcut_item_dialog_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//span[contains(text(),'Naam')]/ancestor::div[contains(@class, 'FieldTextStateless__Wrapper-ynbdsh-0')] //input[contains(@class, 'Input__InputElement-sc-1o6bj35-0')]"))

    @property
    def add_project_shortcut_item_span(self) -> BaseElement:
        return self.get_base_element_child(self._add_project_shortcut_item_span, self.add_shortcut_item_dialog_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//footer //button //span[contains(text(),'Toevoegen')]"))

    def project_item_type_header_h4(self, project_item_type) -> BaseElement:
        return self.get_base_element_child(self._project_item_type_header_h4,
                                           self.add_item_dialog_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//h4[contains(text(),'" + project_item_type + "')]/ancestor::div[2] //button"))

    def add_new_project_shortcut_item(self, project_item: ProjectItem):
        self.project_shortcut_item_web_address_input.send_keys(project_item.url)

        InputHelper.clear_input_value_with_delete_key(self.project_shortcut_item_name_input, Keys.BACKSPACE)
        # while self.project_shortcut_item_name_input.get_attribute('value'):
        #     self.project_shortcut_item_name_input.send_keys(Keys.BACKSPACE)
        self.project_shortcut_item_name_input.send_keys(project_item.name)

    def click_on_add_project_shortcut_item(self, project_item: ProjectItem):
        self.project_item_type_header_h4(project_item.type).click()

    def create_project_shortcut_item(self):
        self.add_project_shortcut_item_span.click()
