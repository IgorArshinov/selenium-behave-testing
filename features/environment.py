from selenium import webdriver

# def before_all(context):
from library.data.data import TestScenarios
from library.pages import *


def before_scenario(context, scenario):
    options = webdriver.ChromeOptions()
    options.add_argument('no-sandbox')
    options.add_argument('window-size=1920,1080')
    options.add_argument('start-maximized')
    options.add_argument('headless')
    options.add_argument('disk-cache-dir=C:/art/PyCharm/projects/TTL/project/cache/')
    chrome_prefs = {'disk-cache-size': 52428800}

    options.experimental_options["prefs"] = chrome_prefs

    chrome_prefs["profile.default_content_settings"] = {"images": 2}

    chrome_prefs["profile.managed_default_content_settings"] = {"images": 2}

    context.browser = webdriver.Chrome(chrome_options=options)
    context.project = None
    context.project_shortcut_item = None
    context.project_epic = None
    context.session_user = None


def after_scenario(context, scenario):
    test_scenario = scenario.name

    if TestScenarios.TS1_001.value in test_scenario or TestScenarios.TS2_001.value in test_scenario or TestScenarios.TS3_001.value in test_scenario or TestScenarios.TS4_001.value in test_scenario or TestScenarios.TS5_001.value in test_scenario:
        project = context.project
        if project:
            projects_page = ProjectsPage(context)
            context.browser.get(projects_page.base_url)
            projects_page.go_to_project_settings_through_table(project)
            project_details_page = ProjectDetailsPage(context, project)
            project_details_page.delete_project()
            context.project = None

    elif TestScenarios.TS2_001.value in test_scenario:
        if context.project_shortcut_item:
            context.project_shortcut_item = None
    elif TestScenarios.TS3_001.value in test_scenario:
        if context.project_epic:
            context.project_epic = None

    context.session_user = None
    context.browser.quit()

# def after_all(context):
