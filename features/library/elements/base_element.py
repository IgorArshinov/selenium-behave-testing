import traceback

from selenium.common.exceptions import TimeoutException, StaleElementReferenceException, NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from library.utilities import LocatorTuple


class BaseElement(object):
    def __init__(self, driver=None, locator=LocatorTuple(by=None, value=None), **kwargs):
        self.driver = driver
        self.locator: LocatorTuple = locator
        self.timeout = 30
        self.web_element = None
        if kwargs.get('web_element'):
            self.web_element: WebElement = kwargs.get('web_element')

    @property
    def web_element(self) -> WebElement:
        return self._web_element

    def wait_and_get_web_element(self) -> WebElement:
        if not self.web_element:
            try:
                # self._web_element = self.driver.find_element(*self.locator)
                self.web_element = WebDriverWait(self.driver, self.timeout).until(
                    EC.visibility_of_element_located(locator=self.locator)
                )
            except(TimeoutException, StaleElementReferenceException):
                traceback.print_exc()

        return self.web_element

    def refresh_and_get_web_element(self) -> WebElement:
        ActionChains(self.driver).pause(1.5).perform()
        self.web_element = None
        try:
            # self._web_element = self.driver.find_element(*self.locator)
            self.web_element = WebDriverWait(self.driver, self.timeout).until(
                EC.visibility_of_element_located(locator=self.locator)
            )
        except(TimeoutException, StaleElementReferenceException):
            traceback.print_exc()

        return self.web_element

    @web_element.setter
    def web_element(self, value: WebElement):
        self._web_element = value

    def find_child(self, locator: LocatorTuple):
        element = None

        if self.child_exists(locator):

            try:
                element = WebDriverWait(self.driver, self.timeout).until(
                    lambda driver: self.wait_and_get_web_element().find_element(locator.by, locator.value)
                )
            except(TimeoutException, StaleElementReferenceException):
                traceback.print_exc()

        return BaseElement(
            driver=self.driver,
            locator=locator,
            web_element=element
        )

    def send_keys(self, text):
        self.wait_and_get_web_element().send_keys(text)

    def click(self):
        self.wait_and_get_web_element().click()

    def clear(self):
        self.wait_and_get_web_element().clear()

    def wait_and_click(self):
        try:
            self.web_element = WebDriverWait(self.driver, self.timeout).until(
                EC.element_to_be_clickable(locator=self.locator)
            )
        except(TimeoutException, StaleElementReferenceException):
            traceback.print_exc()

        self.web_element.click()

    def get_attribute(self, attribute_name):
        return self.wait_and_get_web_element().get_attribute(attribute_name)

    @property
    def text(self):
        return self.wait_and_get_web_element().text

    def child_exists(self, locator: LocatorTuple):
        try:
            self.wait_and_get_web_element().find_element(*locator)
            return True
        except(NoSuchElementException, StaleElementReferenceException):
            return False

    # def web_element_exists_on_webpage(self):
    #     try:
    #         self.driver.find_element(self.locator)
    #         return True
    #     except(NoSuchElementException):
    #         return False
