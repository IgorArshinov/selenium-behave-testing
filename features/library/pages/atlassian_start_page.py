from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from library.elements import BaseElement
from library.pages.base_page import BasePage
from library.utilities import LocatorTuple


class AtlassianStartPage(BasePage):
    def __init__(self, context):
        BasePage.__init__(
            self,
            context.browser,
            base_url='https://start.atlassian.com/')
        self._home_page_content_div = BaseElement()
        self._jira_link_div = BaseElement()

    @property
    def home_page_content_div(self) -> BaseElement:
        # if not self._home_page_content_div:
        #     print("get_base_element_child")
        #     self._home_page_content_div = BaseElement(
        #         driver=self.driver,
        #         locator=LocatorTuple(by=By.CSS_SELECTOR, value="div[data-test-id='home-page-content']"),
        #         web_element=WebDriverWait(self.driver, self.timeout).until(
        #             lambda driver: self.driver.find_element(By.CSS_SELECTOR,
        #                                                     "div[data-test-id='home-page-content']")
        #         ))
        # return self._home_page_content_div
        # return BaseElement(
        #     driver=self.driver,
        #     locator=LocatorTuple(by=By.CSS_SELECTOR, value="div[data-test-id='home-page-content']"),
        #     web_element=WebDriverWait(self.driver, self.timeout).until(
        #         lambda driver: self.driver.find_element(By.CSS_SELECTOR,
        #                                                 "div[data-test-id='home-page-content']")
        #     ))
        return self.get_base_element(self._home_page_content_div, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR, value="div[data-test-id='home-page-content']")
        ))

    @property
    def jira_link_div(self) -> BaseElement:
        # return self.get_base_element(self._jira_link_div, BaseElement(
        #     driver=self.driver,
        #     locator=LocatorTuple(by=By.XPATH, value="//a //div[contains(text(),'Jira Software')]")
        # ))

        return self.get_base_element_child(self._jira_link_div, self.home_page_content_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//a //div[contains(text(),'Jira Software')]"))

    @property
    def jira_link_div_obj(self):
        # return WebDriverWait(self.driver, self.timeout).until(
        #     EC.visibility_of_element_located((By.XPATH, "//a //div[contains(text(),'Jira Software')]"))
        # )
        # return BaseElement(
        #     driver=self.driver,
        #     locator=LocatorTuple(by=By.CSS_SELECTOR, value="//a //div[contains(text(),'Jira Software')]"),
        #     # web_element=WebDriverWait(self.driver, self.timeout).until(
        #     #     lambda driver: self.home_page_content_div.web_element.find_element(By.XPATH,
        #     #                                                                        "//a //div[contains(text(),'Jira Software')]")
        #     # ))
        #     web_element=WebDriverWait(self.driver, self.timeout).until(
        #         lambda driver: self.driver.find_element(By.XPATH,
        #                                                 "//a //div[contains(text(),'Jira Software')]")
        #     ))

        # return self.driver.find_element(By.XPATH, "//a //div[contains(text(),'Jira Software')]")
        # return self.home_page_content_div.web_element.find_element(By.XPATH, "//a //div[contains(text(),'Jira Software')]")
        if not self._jira_link_div:
            self._jira_link_div = BaseElement(
                driver=self.driver,
                locator=LocatorTuple(by=By.CSS_SELECTOR, value="//a //div[contains(text(),'Jira Software')]"),
                # web_element=WebDriverWait(self.driver, self.timeout).until(
                #     lambda driver: self.home_page_content_div.web_element.find_element(By.XPATH,
                #                                                                        "//a //div[contains(text(),'Jira Software')]")
                # ))
                web_element=WebDriverWait(self.driver, self.timeout).until(
                    lambda driver: self.driver.find_element(By.XPATH,
                                                            "//a //div[contains(text(),'Jira Software')]")
                ))
        return self._jira_link_div

    def navigate_to_jira_software(self):
        # print(timeit(lambda: self.jira_link_div_obj, number=1000))
        # print(timeit(lambda: self.jira_link_div.web_element, number=1000))
        # print(self.jira_link_div.web_element_exists())
        # print(self.jira_link_div.web_element)
        # print(id(self.jira_link_div))
        # print(id(self.jira_link_div))
        # print(id(self.jira_link_div))
        self.jira_link_div.click()
        # print(self.jira_link_div.web_element)
        # assert False

    # def navigate_to_jira_software2(self):
    # self.jira_link_div_obj.click()
    # print(id(self.jira_link_div_obj))
    # print(id(self.jira_link_div_obj))
    # assert False
