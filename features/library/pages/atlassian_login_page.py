from selenium.webdriver.common.by import By

from library.data import User
from library.elements import BaseElement
from library.pages.base_page import BasePage
from library.utilities import LocatorTuple


class AtlassianLoginPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(
            self,
            context.browser,
            base_url='https://id.atlassian.com/login')
        self._username_input = BaseElement()
        self._password_input = BaseElement()
        self._login_submit_button = BaseElement()

    @property
    def username_input(self) -> BaseElement:
        return self.get_base_element(self._username_input, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR, value="input[id='username']")
        ))

    @property
    def password_input(self) -> BaseElement:
        return self.get_base_element(self._password_input, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR, value="input[id='password']")
        ))

    @property
    def login_submit_button(self) -> BaseElement:
        return self.get_base_element(self._login_submit_button, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR, value="button[id='login-submit']")
        ))

    def enter_credentials(self, user: User):
        self.username_input.send_keys(user.username)
        self.login_submit_button.click()
        self.password_input.send_keys(user.password)

    def login(self):
        self.login_submit_button.click()
