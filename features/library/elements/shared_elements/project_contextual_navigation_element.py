from library.data import Project, ProjectItem
from library.elements import BaseElement
from library.pages import BasePage
from selenium.webdriver.common.by import By
from library.utilities import LocatorTuple


class ProjectContextualNavigationElement(BaseElement, BasePage):
    current_project = Project()

    def __init__(self, context, project):
        BaseElement.__init__(
            self,
            context.browser,
            LocatorTuple(by=By.CSS_SELECTOR,
                         value="div[data-testid='ContextualNavigation']"),
        )
        self._pages_div = BaseElement()
        self._project_item_navigation_item_div = BaseElement()
        self._project_item_link_navigation_item_a = BaseElement()
        self._project_item_name_navigation_item_div = BaseElement()
        self._next_gen_project_div = BaseElement()
        self._project_name_span = BaseElement()
        self._add_item_div = BaseElement()
        self._roadmap_div = BaseElement()
        self.current_project = project

    @property
    def next_gen_project_div(self) -> BaseElement:
        return self.get_base_element_child(self._next_gen_project_div, self,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//div[text() = 'Je bevindt je in een next-gen project']"))

    @property
    def project_name_span(self) -> BaseElement:
        return self.get_base_element_child(self._project_name_span, self,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//div[@data-test-id='navigation-apps.project-switcher-v2'] //div[@class='css-1ier59u' and contains(text(),'" + self.current_project.name + "')]"))

    @property
    def roadmap_div(self) -> BaseElement:
        return self.get_base_element_child(self._roadmap_div, self,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="a[data-testid='NavigationItem' ][id='roadmap']"))

    @property
    def add_item_div(self) -> BaseElement:
        return self.get_base_element_child(self._add_item_div, self,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//button[@data-testid='NavigationItem'] //div[@class='css-1olrtn' and contains(text(), 'Item toevoegen')]"))

    def project_item_navigation_item_div(self, project_shortcut_item: ProjectItem) -> BaseElement:
        return self.get_base_element_child(self._project_item_navigation_item_div,
                                           self.project_item_link_navigation_item_a(project_shortcut_item),
                                           LocatorTuple(by=By.XPATH,
                                                        value="//div[@class='css-1olrtn' and (text() = '" + project_shortcut_item.name + "')]"))

    def project_item_link_navigation_item_a(self, project_shortcut_item: ProjectItem) -> BaseElement:
        return self.get_base_element_child(self._project_item_link_navigation_item_a,
                                           self,
                                           LocatorTuple(by=By.XPATH,
                                                        value="(//a[@data-testid='NavigationItem' and @href='" + project_shortcut_item.url + "']) [last()] "))

    @property
    def pages_div(self) -> BaseElement:
        return self.get_base_element_child(self._pages_div, self,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="a[data-testid='NavigationItem' ][id='projectpages']"))

    def navigate_to_roadmap_page(self):
        self.roadmap_div.click()

    def add_new_project_item(self):
        self.add_item_div.click()

    def navigate_to_pages_page(self):
        self.pages_div.click()

    def validate_project_name_matches(self, project: Project):
        return project.name in self.project_name_span.text

    def validate_project_type_matches(self, project: Project):
        return project.type.lower() in self.next_gen_project_div.text.lower()

    def validate_project_shortcut_item_name_matches(self, project_shortcut_item: ProjectItem):
        return project_shortcut_item.name in self.project_item_navigation_item_div(project_shortcut_item).text

    def validate_project_shortcut_item_url_matches(self, project_shortcut_item: ProjectItem):
        return project_shortcut_item.url in self.project_item_link_navigation_item_a(
            project_shortcut_item).get_attribute("href")
