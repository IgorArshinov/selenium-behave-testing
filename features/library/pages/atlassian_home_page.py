
from selenium.webdriver.common.by import By

from library.elements import BaseElement
from library.pages.base_page import BasePage
from library.utilities import LocatorTuple


class AtlassianHomePage(BasePage):

    def __init__(self, context):
        BasePage.__init__(
            self,
            context.browser,
            base_url='https://www.atlassian.com/nl')
        self._login_hyperlink = BaseElement()

    @property
    def login_hyperlink(self) -> BaseElement:
        return self.get_base_element(self._login_hyperlink, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR, value="a[class='global-nav--wac__button--login'][ id='gray_link']")
        ))

    @property
    def cookies_banner_close_button(self) -> BaseElement:
        return self.get_base_element(self._login_hyperlink, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="button[class='optanon-alert-box-close banner-close-button']")
        ))

    def navigate_to_login_page(self):
        if self.cookies_banner_close_button.wait_and_get_web_element():
            self.click_with_javascript(self.cookies_banner_close_button.wait_and_get_web_element())

        self.login_hyperlink.wait_and_click()
