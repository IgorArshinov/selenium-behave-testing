from selenium.webdriver.common.by import By

from library.elements import BaseElement
from library.elements.shared_elements import ProjectContextualNavigationElement, DialogElement
from library.pages import BasePage
from library.utilities import LocatorTuple


class ProjectBoardPage(BasePage):

    def __init__(self, context, project):
        BasePage.__init__(
            self,
            context.browser,
            base_url='https://ttljsp.atlassian.net/projects/' + project.code)
        self._projects_link_a = BaseElement()
        self.project_contextual_navigation_element = ProjectContextualNavigationElement(
            context, project)
        self.current_project = project
        self.dialog_element = DialogElement(context)

    @property
    def projects_link_a(self) -> BaseElement:
        return self.get_base_element(self._projects_link_a, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="div[data-test-id='software-board.board'] a[href='/secure/BrowseProjects.jspa']")
        ))

    def navigate_to_projects_page(self):
        self.projects_link_a.click()
