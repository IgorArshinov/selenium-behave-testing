Feature: Editing epic

  Scenario Outline: Validate epic details container with valid input

  As an Agile team member, I can modify my epic inside my project, so that my team knows that my epic is changed.

    Given I navigate to "Atlassian Home Page"
    And I click on link Login
    And I enter <User> credentials
    And I click on button Inloggen
    And I click on Jira Software
    And I click on button Project aanmaken to create <Project>
    And I click on button Next-gen selecteren
    And I create Next-gen project using valid values
    And I click on button Maken
    And I click on button Roadmap
    And I create project epic using <Epic> values
    And I press Enter
    And I click on created project epic
    When I change project epic details using <Updated Epic> values
    And I click on X
    Then I validate that updated project epic exists on roadmap
    And I validate that project epic details are updated

    Examples:
      | User            | Project                                   | Epic          | Updated Epic       |
      | "Igor Arshinov" | "Valid Next-gen private Kanban project"   | "Purple Epic" | "Orange Epic"      |
      | "Igor Arshinov" | "Valid Next-gen public Scrum project"     | "Purple Epic" | "Dark Orange Epic" |
      | "Igor Arshinov" | "Valid Next-gen restricted Scrum project" | "Purple Epic" | "Blue Epic"        |


