Feature: Contextual menu navigation

  Scenario Outline: Validate Paginas button in Contextual Navigation menu of project

  As an Agile team member, I can navigate to my Next-gen project’s pages webpage, so that I can access our project’s documents.

    Given I navigate to "Atlassian Home Page"
    And I click on link Login
    And I enter <User> credentials
    And I click on button Inloggen
    And I click on Jira Software
    And I click on button Project aanmaken to create <Project>
    And I click on button Next-gen selecteren
    And I create Next-gen project using valid values
    And I click on button Maken
    When I click on button Paginas
    Then I am on Pages page

    Examples:
      | User            | Project                                   |
      | "Igor Arshinov" | "Valid Next-gen private Kanban project"   |
      | "Igor Arshinov" | "Valid Next-gen public Scrum project"     |
      | "Igor Arshinov" | "Valid Next-gen restricted Scrum project" |
