class InputHelper:

    @staticmethod
    def clear_input_value_with_delete_key(base_element, key):
        while base_element.get_attribute('value'):
            base_element.send_keys(key)
