import traceback

from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver import ActionChains
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait

from library.elements import BaseElement
from library.elements import EC
from library.utilities import LocatorTuple


class BasePage(object):

    def __init__(self, browser, base_url='https://www.atlassian.com/nl'):
        self.base_url = base_url
        self.driver = browser
        self.timeout = 10

    def visit(self, url):
        self.driver.get(url)

    def hover(self, element):
        ActionChains(self.driver).move_to_element(element).perform()
        # I don't like this but hover is sensitive and needs some sleep time
        # time.sleep(3)

    def hover_and_click(self, element: WebElement):
        ActionChains(self.driver).move_to_element(element).click().perform()
        # I don't like this but hover is sensitive and needs some sleep time
        # time.sleep(3)

    def click_with_javascript(self, element: WebElement):
        # time.sleep(3)
        self.driver.execute_script("arguments[0].click();", element)

    def wait_until_base_element_disappears(self, base_element: BaseElement):
        try:
            return WebDriverWait(self.driver, self.timeout).until(
                EC.invisibility_of_element(base_element.locator)
            )
        except(TimeoutException, StaleElementReferenceException):
            traceback.print_exc()
            return False

    def get_base_element(self, page_property: BaseElement, base_element: BaseElement):
        if page_property.locator.value != base_element.locator.value:
            page_property.driver = base_element.driver
            page_property.locator = base_element.locator
            # page_property._web_element = WebDriverWait(base_element.driver, base_element.timeout).until(
            #     lambda driver: base_element.driver.find_element(*base_element.locator)
            # )
        return page_property

    def get_base_element_child(self, page_property: BaseElement, parent_base_element: BaseElement,
                               child_locator: LocatorTuple):
        if page_property.locator.value != child_locator.value:
            child_element = parent_base_element.find_child(child_locator)
            page_property.driver = child_element.driver
            page_property.locator = child_element.locator
            # print("get_base_element_child: " + str(page_property.locator))
            # print("get_base_element_child: " + str(page_property.web_element))
            # print("get_base_element_child: " + str(child_element.web_element))
            page_property.web_element = child_element.web_element
        return page_property
