from .base_page import BasePage
from library.elements import BaseElement
from selenium.webdriver.common.by import By

from library.utilities import LocatorTuple


class ProjectDetailsPage(BasePage):
    def __init__(self, context, project):
        BasePage.__init__(
            self,
            context.browser,
            base_url="https://ttljsp.atlassian.net/jira/software/projects/" + project.code + "/settings/details")
        self.current_project = project

    @property
    def details_menu_button(self) -> BaseElement:
        return BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="div[class*='styled__ActionsWrapper-sc-1a16ki5-4'] button")
        )

    @property
    def delete_project_menu_span(self) -> BaseElement:
        return BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.XPATH,
                                 value="//span[contains(@class, 'ItemParts__Content-sc-14xek3m-5') and contains(text(),'Project verwijderen')]")
        )

    @property
    def dialog_div(self) -> BaseElement:
        return BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="div[role='dialog']")
        )

    @property
    def delete_item_dialog_span(self) -> BaseElement:
        return BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.XPATH,
                                 value="//button //span[contains(text(),'Verwijderen')]")
        )

    def delete_project(self):
        self.details_menu_button.click()
        self.delete_project_menu_span.click()
        self.delete_item_dialog_span.click()
