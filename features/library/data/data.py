import enum
from datetime import date, timedelta


class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password


class ProjectItem:
    def __init__(self, type, url, name):
        self.type: str = type
        self.url: str = url
        self.name: str = name


class ProjectEpic:
    def __init__(self, name, color, description, executor, labels, start_date, end_date, notifier, status):
        self.name: str = name
        self.color: str = color
        self.description: str = description
        self.executor: str = executor
        self.labels: str = labels
        self.start_date: date = start_date
        self.end_date: date = end_date
        self.notifier: str = notifier
        self.status: str = status


class Project:
    def __init__(self, name=None, accessibility_level=None, code=None, template=None, type=None, leader=None):
        self.name: str = name
        self.accessibility_level: str = accessibility_level
        self.code: str = code
        self.template: str = template
        self.type: str = type
        self.leader: str = leader


class Urls(enum.Enum):
    url_atlassian = str('https://www.atlassian.com/nl')

    @staticmethod
    def lookup(environment):
        if environment == "Atlassian Home Page":
            return Urls.url_atlassian


class Users(enum.Enum):
    igor_arshinov = User("igor.arshinov@ttl.be", "testingisfun")

    @staticmethod
    def lookup(user):
        if user == "Igor Arshinov":
            return Users.igor_arshinov


class Projects(enum.Enum):
    valid_next_gen_project_1 = Project("Valid_next_gen_project_1", "Privé", "KEYTEST1", "Kanban", "Next-gen",
                                       "igor.arshinov")
    valid_next_gen_project_2 = Project("Valid_next_gen_project_2", "Open", "KEYTEST2", "Scrum", "Next-gen",
                                       "igor.arshinov")

    valid_next_gen_project_3 = Project("Valid_next_gen_project_3", "Beperkt", "KEYTEST3", "Scrum", "Next-gen",
                                       "igor.arshinov")

    @staticmethod
    def lookup(Project):
        if Project == "Valid Next-gen private Kanban project":
            return Projects.valid_next_gen_project_1
        elif Project == "Valid Next-gen public Scrum project":
            return Projects.valid_next_gen_project_2
        elif Project == "Valid Next-gen restricted Scrum project":
            return Projects.valid_next_gen_project_3


class ProjectItems(enum.Enum):
    valid_project_item_shortcut_1 = ProjectItem("Snelkoppeling", "https://www.atlassian.com/nl",
                                                "Valid_project_item_shortcut_1")
    valid_project_item_shortcut_2 = ProjectItem("Snelkoppeling", "https://www.google.be",
                                                "Valid_project_item_shortcut_2")
    valid_project_item_shortcut_3 = ProjectItem("Snelkoppeling", "https://www.ttl.be",
                                                "Valid_project_item_shortcut_3")

    @staticmethod
    def lookup(ProjectItem):
        if ProjectItem == "Valid Atlassian Shortcut":
            return ProjectItems.valid_project_item_shortcut_1
        elif ProjectItem == "Valid Google Shortcut":
            return ProjectItems.valid_project_item_shortcut_2
        elif ProjectItem == "Valid TTL Shortcut":
            return ProjectItems.valid_project_item_shortcut_3


class ProjectEpics(enum.Enum):
    valid_project_epic_1 = ProjectEpic(
        "Valid_project_epic_1", "#8777D9", "Valid_project_epic_1_description", "Niet toegewezen",
        ["Valid_project_epic_1_label_1", "Valid_project_epic_1_label_2"], date.today() + timedelta(days=1),
                                                                          date.today() + timedelta(days=10),
        "igor.arshinov",
        "Nog doen")

    valid_project_epic_1_updated = ProjectEpic(
        "Valid_project_epic_1_updated", "#FF7452", "Valid_project_epic_1_description_updated", "igor.arshinov",
        ["Valid_project_epic_1_label_1_updated", "Valid_project_epic_1_label_2_updated"],
        date.today() + timedelta(days=11),
        date.today() + timedelta(days=20),
        "Automation for Jira",
        "Gereed")

    valid_project_epic_2_updated = ProjectEpic(
        "Valid_project_epic_2_updated", "#DE350B", "Valid_project_epic_2_description_updated", "igor.arshinov",
        ["Valid_project_epic_2_label_1_updated", "Valid_project_epic_2_label_2_updated"],
        date.today() + timedelta(days=11),
        date.today() + timedelta(days=20),
        "Automation for Jira",
        "Actief")

    valid_project_epic_3_updated = ProjectEpic(
        "Valid_project_epic_3_updated", "#2684FF", "Valid_project_epic_3_description_updated", "igor.arshinov",
        ["Valid_project_epic_3_label_1_updated", "Valid_project_epic_3_label_2_updated"],
        date.today() + timedelta(days=11),
        date.today() + timedelta(days=20),
        "Automation for Jira",
        "Nog doen")

    @staticmethod
    def lookup(ProjectEpic):
        if ProjectEpic == "Purple Epic":
            return ProjectEpics.valid_project_epic_1
        elif ProjectEpic == "Orange Epic":
            return ProjectEpics.valid_project_epic_1_updated
        elif ProjectEpic == "Dark Orange Epic":
            return ProjectEpics.valid_project_epic_2_updated
        elif ProjectEpic == "Blue Epic":
            return ProjectEpics.valid_project_epic_3_updated


class TestScenarios(enum.Enum):
    TS1_001 = "Validate Next-gen Project web form with valid input"
    TS2_001 = "Validate Add Shortcut web form with valid input"
    TS3_001 = "Validate epic details container with valid input"
    TS4_001 = "Validate Next-gen Project details are visible on Projects page"
    TS5_001 = "Validate Paginas button in Contextual Navigation menu of project"
