from selenium.webdriver.common.by import By

from library.data import Project
from library.elements import BaseElement
from library.utilities import LocatorTuple
from .base_page import BasePage


class ProjectsPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(
            self,
            context.browser,
            base_url='https://ttljsp.atlassian.net/secure/BrowseProjects.jspa')
        self._specific_project_leader_div = BaseElement()
        self._specific_project_name_span = BaseElement()
        self._specific_project_code_div = BaseElement()
        self._project_settings_menu_link_a = BaseElement()
        self._project_settings_button = BaseElement()
        self._project_with_code_table_row = BaseElement()
        self._project_with_name_table_row = BaseElement()
        self._project_with_code_and_name_table_row = BaseElement()
        self._project_name_span = BaseElement()
        self._link_with_project_code_a = BaseElement()
        self._projects_table_container_div = BaseElement()
        self._accessibility_level_div = BaseElement()
        self._change_template_button = BaseElement()
        self._accessibility_level_button = BaseElement()
        self._project_name_input = BaseElement()
        self._select_next_gen_project_button = BaseElement()
        self._template_picker_div = BaseElement()
        self._create_new_project_div = BaseElement()
        self._create_project_button = BaseElement()
        self._code_input = BaseElement()
        self._create_button = BaseElement()

    @property
    def create_project_button(self) -> BaseElement:
        return self.get_base_element(self._create_project_button, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="button[data-test-id='global-pages.directories.projects-directory-v2.create-projects-button.button.button']")
        ))

    @property
    def create_new_project_div(self) -> BaseElement:
        return self.get_base_element(self._create_project_button, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="div[class='css-114lpln']")
        ))

    def template_picker_div(self, template) -> BaseElement:
        return self.get_base_element(self._template_picker_div, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.XPATH,
                                 value="//h3[contains(text(),'" + template + "')]")
        ))

    @property
    def select_next_gen_project_button(self) -> BaseElement:
        return self.get_base_element_child(self._select_next_gen_project_button, self.create_new_project_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//button[@data-test-id='project-create.experience-selection-v2.column.footer.next-gen-footer-button'] //span[contains(text(),'Next-gen selecteren')]"))

    @property
    def project_name_input(self) -> BaseElement:
        return self.get_base_element_child(self._project_name_input, self.create_new_project_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="input[class='css-1rmy9fa"))

    @property
    def accessibility_level_button(self) -> BaseElement:
        return self.get_base_element_child(self._accessibility_level_button, self.create_new_project_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="button[data-test-id='project-access.field.button-trigger']"))

    @property
    def change_template_button(self) -> BaseElement:
        return self.get_base_element_child(self._change_template_button, self.create_new_project_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="button[id='project-create.create-form.change-template-button']"))

    @property
    def create_button(self) -> BaseElement:
        return self.get_base_element_child(self._create_button, self.create_new_project_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//button //span[contains(text(),'Maken')]"))

    @property
    def code_input(self) -> BaseElement:
        self._code_input = self.get_base_element_child(self._code_input, self.create_new_project_div,
                                                       LocatorTuple(by=By.CSS_SELECTOR,
                                                                    value="input[class*='Input__InputElement-sc-1o6bj35-0']"))

        self._code_input.clear()
        return self._code_input

    def accessibility_level_div(self, accessibility_level) -> BaseElement:
        self.accessibility_level_button.click()
        return self.get_base_element_child(self._accessibility_level_div,
                                           self.create_new_project_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//div[contains(@class, 'Droplist__Content-sc-1z05y4v-1')] //div[contains(text(),'" + accessibility_level + "')]"))

    def select_next_gen_project(self):
        self.select_next_gen_project_button.click()

    def fill_in_next_gen_project_web_form(self, project: Project):
        self.project_name_input.send_keys(project.name)
        self.accessibility_level_div(project.accessibility_level).click()
        self.code_input.send_keys(project.code)
        self.change_template_button.click()
        self.template_picker_div(project.template).click()

    def create_next_gen_project(self):
        self.create_button.click()

    @property
    def projects_table_container_div(self) -> BaseElement:
        return self.get_base_element(self._projects_table_container_div, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="div[data-test-id='global-pages.directories.directory-base.content.table.container']")
        ))

    def link_with_project_code_a(self, project_code) -> BaseElement:
        return self.get_base_element_child(self._link_with_project_code_a,
                                           self.projects_table_container_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//tr //td //a[@href='/browse/" + project_code + "']"))

    def project_name_span(self, project_name) -> BaseElement:
        return self.get_base_element_child(self._project_name_span,
                                           self.projects_table_container_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//tr //td //span[(text() = '" + project_name + "')]"))

    def specific_project_name_span(self, project) -> BaseElement:
        return self.get_base_element_child(self._specific_project_name_span,
                                           self.project_with_code_and_name_table_row(project.code, project.name),
                                           LocatorTuple(by=By.XPATH,
                                                        value="//td //span[(text() = '" + project.name + "')]"))

    def specific_project_code_div(self, project) -> BaseElement:
        return self.get_base_element_child(self._specific_project_code_div,
                                           self.project_with_code_and_name_table_row(project.code, project.name),
                                           LocatorTuple(by=By.XPATH,
                                                        value="//td //div[(text() = '" + project.code + "')]"))

    def specific_project_type_div(self, project) -> BaseElement:
        return self.get_base_element_child(self._specific_project_code_div,
                                           self.project_with_code_and_name_table_row(project.code, project.name),
                                           LocatorTuple(by=By.XPATH,
                                                        value="//td //div[(text() = '" + project.type + " software')]"))

    def specific_project_leader_div(self, project) -> BaseElement:
        return self.get_base_element_child(self._specific_project_leader_div,
                                           self.project_with_code_and_name_table_row(project.code, project.name),
                                           LocatorTuple(by=By.XPATH,
                                                        value="//td //a //div[(text() = '" + project.leader + "')]"))

    def project_with_code_and_name_table_row(self, project_code, project_name) -> BaseElement:
        return self.get_base_element_child(self._project_with_code_and_name_table_row,
                                           self.projects_table_container_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//tr //td //a[@href='/browse/" + project_code + "'] //span[text() = '" + project_name + "']/ancestor::tr"))

    def project_with_name_table_row(self, project_name) -> BaseElement:
        return self.get_base_element_child(self._project_with_name_table_row,
                                           self.projects_table_container_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//tr //td //div[text() = '" + project_name + "']/ancestor::tr"))

    def project_with_code_table_row(self, project_code) -> BaseElement:
        return self.get_base_element_child(self._project_with_code_table_row,
                                           self.projects_table_container_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//tr //td //a[@href='/browse/" + project_code + "']/ancestor::tr"))

    def project_settings_button(self, project_row) -> BaseElement:
        return self.get_base_element_child(self._project_settings_button, project_row,
                                           LocatorTuple(by=By.CSS_SELECTOR, value="td:last-child button"))

    def project_settings_menu_link_a(self, project_row) -> BaseElement:
        return self.get_base_element_child(self._project_settings_menu_link_a, project_row,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="a[href^='/jira/software/projects/']"))

    def create_new_project(self):
        self.create_project_button.click()

    def project_code_exists_in_table(self, project_code):
        if self.link_with_project_code_a(project_code).web_element:
            return True

    def project_name_exists_in_table(self, project_name):
        if self.project_name_span(project_name).web_element:
            return True

    def go_to_project_settings_through_table(self, project: Project):
        project_row = self.project_with_code_and_name_table_row(project.code, project.name)

        self.__click_on_project_settings_in_table(project_row)

    def go_to_project_settings_through_table_by_name(self, project_name):
        project_row = self.project_with_name_table_row(project_name)

        self.__click_on_project_settings_in_table(project_row)

    def go_to_project_settings_through_table_by_code(self, project_code):
        project_row = self.project_with_code_table_row(project_code)

        self.__click_on_project_settings_in_table(project_row)

    def __click_on_project_settings_in_table(self, project_row: BaseElement):
        self.project_settings_button(project_row).click()
        self.project_settings_menu_link_a(project_row).click()

    def validate_next_gen_project_details_are_visible(self, project: Project):
        assert project.name in self.specific_project_name_span(project).text
        assert project.type in self.specific_project_type_div(project).text

        assert project.code in self.specific_project_code_div(project).text

        return project.leader in self.specific_project_leader_div(project).text
