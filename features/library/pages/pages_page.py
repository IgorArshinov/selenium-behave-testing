from selenium.webdriver.common.by import By

from library.elements import BaseElement
from library.elements.shared_elements import ProjectContextualNavigationElement
from library.utilities import LocatorTuple
from .base_page import BasePage


class PagesPage(BasePage):

    def __init__(self, context, project):
        BasePage.__init__(
            self,
            context.browser,
            base_url='https://ttljsp.atlassian.net/projects/' + project.code)
        self._projects_link_a = BaseElement()
        self.project_contextual_navigation_element = ProjectContextualNavigationElement(
            context, project)
        self.current_project = project

    @property
    def pages_header_h1(self) -> BaseElement:
        return self.get_base_element(self._projects_link_a, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.XPATH,
                                 value='//div[@data-testid="Content"] //h1[text() ="Pagina\'s"]')
        ))

    def validate_user_is_on_pages_page(self):
        return 'Pagina\'s' in self.pages_header_h1.text
