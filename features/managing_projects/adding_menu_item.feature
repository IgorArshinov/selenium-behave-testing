Feature: Adding menu item

  Scenario Outline: Validate Add Shortcut web form with valid input

  As an Agile team member, I can add my shortcut to the Contextual Navigation Menu of my project, so that my team can quickly access the specific webpage.

    Given I navigate to "Atlassian Home Page"
    And I click on link Login
    And I enter <User> credentials
    And I click on button Inloggen
    And I click on Jira Software
    And I click on button Project aanmaken to create <Project>
    And I click on button Next-gen selecteren
    And I create Next-gen project using valid values
    And I click on button Maken
    When I click on button Item toevoegen to create <Project Item Shortcut>
    And I click on Snelkoppeling toevoegen
    And I create project item using valid values
    And I click on button Toevoegen
    Then I validate that project item for project is created

    Examples:
      | User            | Project                                   | Project Item Shortcut      |
      | "Igor Arshinov" | "Valid Next-gen private Kanban project"   | "Valid Atlassian Shortcut" |
      | "Igor Arshinov" | "Valid Next-gen public Scrum project"     | "Valid Google Shortcut"    |
      | "Igor Arshinov" | "Valid Next-gen restricted Scrum project" | "Valid TTL Shortcut"       |
