from behave import use_step_matcher, given, then, step
from library.data import *
from library.pages import *

use_step_matcher("parse")


@given(u'I navigate to "{website}"')
def visit_website(context, website):
    page = AtlassianHomePage(context)
    context.home_page = data.Urls.lookup(website).value
    page.visit(context.home_page)


@step(u'I click on link Login')
def login_atlassian(context):
    page = AtlassianHomePage(context)
    page.navigate_to_login_page()


@step(u'I enter "{user}" credentials')
def login_atlassian(context, user):
    context.session_user = data.Users.lookup(user).value

    page = AtlassianLoginPage(context)
    page.enter_credentials(context.session_user)


@step(u'I click on button Inloggen')
def login_atlassian(context):
    page = AtlassianLoginPage(context)
    page.login()


@step(u'I click on Jira Software')
def click_div_jira_software(context):
    page = AtlassianStartPage(context)
    page.navigate_to_jira_software()


@step(u'I click on button Project aanmaken to create "{project_to_create}"')
def click_create_project_button(context, project_to_create):
    context.project = data.Projects.lookup(project_to_create).value
    project = context.project
    page = ProjectsPage(context)

    if page.project_code_exists_in_table(project.code):
        page.go_to_project_settings_through_table_by_code(project.code)
        project_details_page = ProjectDetailsPage(context, project)
        project_details_page.delete_project()

    if page.project_name_exists_in_table(project.name):
        page.go_to_project_settings_through_table_by_name(project.name)
        project_details_page = ProjectDetailsPage(context, project)
        project_details_page.delete_project()
    page.create_new_project()


@step(u'I click on button Next-gen selecteren')
def click_select_next_gen_button(context):
    page = ProjectsPage(context)
    page.select_next_gen_project()


@step(u'I create Next-gen project using valid values')
def create_next_gen_project_with_values(context):
    page = ProjectsPage(context)
    page.fill_in_next_gen_project_web_form(context.project)


@step(u'I click on button Maken')
def create_next_gen_project_with_values(context):
    page = ProjectsPage(context)
    page.create_next_gen_project()


@step(u'I click on button Item toevoegen to create "{project_item_to_create}"')
def click_add_item(context, project_item_to_create):
    context.project_shortcut_item = data.ProjectItems.lookup(project_item_to_create).value

    page = ProjectBoardPage(context, context.project)
    page.project_contextual_navigation_element.add_new_project_item()


@step(u'I click on Snelkoppeling toevoegen')
def click_add_item(context):
    page = ProjectBoardPage(context, context.project)
    page.dialog_element.click_on_add_project_shortcut_item(context.project_shortcut_item)


@step(u'I create project item using valid values')
def create_shortcut_with_valid_values(context):
    page = ProjectBoardPage(context, context.project)
    page.dialog_element.add_new_project_shortcut_item(context.project_shortcut_item)


@step(u'I click on button Toevoegen')
def create_shortcut_with_valid_values(context):
    page = ProjectBoardPage(context, context.project)
    page.dialog_element.create_project_shortcut_item()


@step(u'I click on button Roadmap')
def click_roadmap(context):
    # context.project = data.Projects.lookup("Valid_next_gen_project_1").value
    page = ProjectBoardPage(context, context.project)
    page.project_contextual_navigation_element.navigate_to_roadmap_page()


@step(u'I create project epic using "{project_epic_data}" values')
def create_epic_with_values(context, project_epic_data):
    context.project_epic = data.ProjectEpics.lookup(project_epic_data).value

    project_epic = context.project_epic
    page = ProjectRoadmapPage(context, context.project)
    if not page.project_epic_name_exists_in_table(project_epic.name):
        page.add_new_project_epic(project_epic)


@step(u'I press Enter')
def create_epic_with_values(context):
    page = ProjectRoadmapPage(context, context.project)
    if not page.project_epic_name_exists_in_table(context.project_epic.name):
        page.create_new_project_epic()


@step(u'I click on created project epic')
def create_epic_with_values(context):
    page = ProjectRoadmapPage(context, context.project)
    page.click_on_project_epic_name(context.project_epic.name)


@step(u'I change project epic details using "{project_epic_updated_data}" values')
def change_project_epic_details(context, project_epic_updated_data):
    context.project_epic_updated = data.ProjectEpics.lookup(project_epic_updated_data).value
    page = ProjectRoadmapPage(context, context.project)
    page.change_project_epic_details(context.project_epic_updated)


@step(u'I click on X')
def create_epic_with_values(context):
    page = ProjectRoadmapPage(context, context.project)
    page.close_project_epic_details()


@step(u'I click on link Projecten')
def click_select_next_gen_button(context):
    page = ProjectBoardPage(context, context.project)
    page.navigate_to_projects_page()


@step(u'I click on button Paginas')
def click_select_next_gen_button(context):
    page = ProjectBoardPage(context, context.project)
    page.project_contextual_navigation_element.navigate_to_pages_page()


@then(u'I validate that project item for project is created')
def validate_that_shortcut_item_is_created_for_existing_project(context):
    project_shortcut_item = context.project_shortcut_item
    page = ProjectBoardPage(context, context.project)
    assert page.project_contextual_navigation_element.validate_project_shortcut_item_name_matches(project_shortcut_item)
    assert page.project_contextual_navigation_element.validate_project_shortcut_item_url_matches(project_shortcut_item)


@then(u'I validate that Next-gen project is created')
def validate_that_next_gen_project_is_created(context):
    next_gen_project = context.project
    page = ProjectBoardPage(context, context.project)
    assert page.project_contextual_navigation_element.validate_project_name_matches(next_gen_project)
    assert page.project_contextual_navigation_element.validate_project_type_matches(next_gen_project)


@then(u'I validate that updated project epic exists on roadmap')
def validate_that_project_epic_exists_on_roadmap(context):
    page = ProjectRoadmapPage(context, context.project)
    assert page.validate_project_epic_exists_on_roadmap(
        context.project_epic_updated)


@then(u'I validate that project epic details are updated')
def validate_that_project_epic_details_are_updated(context):
    page = ProjectRoadmapPage(context, context.project)
    page.click_on_project_epic_name(context.project_epic_updated.name)
    assert page.validate_project_epic_details_are_updated(
        context.project_epic_updated)


@then(u'I can see details of created Next-gen project')
def validate_that_project_epic_details_are_updated(context):
    page = ProjectsPage(context)
    assert page.validate_next_gen_project_details_are_visible(context.project)


@then(u'I am on Pages page')
def validate_that_project_epic_details_are_updated(context):
    page = PagesPage(context, context.project)
    assert page.validate_user_is_on_pages_page()
