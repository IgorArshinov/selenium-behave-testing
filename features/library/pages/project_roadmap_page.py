from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from library.data import ProjectEpic
from library.elements import BaseElement
from library.pages import BasePage
from library.utilities import InputHelper
from library.utilities import LocatorTuple


class ProjectRoadmapPage(BasePage):
    def __init__(self, context, project):
        BasePage.__init__(
            self,
            context.browser,
            base_url='https://ttljsp.atlassian.net/projects/' + project.code)

        # Python objects already store attributes in a dict so what is the point of using yet another dict here ?
        # https://stackoverflow.com/questions/20561002/how-to-associate-a-property-like-object-with-a-dict-value-in-python
        self._create_epic_spinner_svg = BaseElement()
        self._project_epic_container_div = BaseElement()
        self._epic_details_status_field_wrapper_div = BaseElement()
        self._epic_details_color_div = BaseElement()
        self._epic_details_container_div = BaseElement()
        self._epic_details_div = BaseElement()
        self._create_epic_button = BaseElement()
        self._create_epic_textarea = BaseElement()
        self._table_content_list_div = BaseElement()
        self._project_name_p = BaseElement()
        self._epic_details_close_span = BaseElement()
        self._epic_details_notifier_select_input = BaseElement()
        self._epic_details_notifier_field_div = BaseElement()
        self._epic_details_end_date_table_select_date_div = BaseElement()
        self._epic_details_end_date_input_select_input = BaseElement()
        self._epic_details_end_date_input_field_wrapper_div = BaseElement()
        self._epic_details_start_date_input_select_input = BaseElement()
        self._start_date_input_field_wrapper_div = BaseElement()
        self._epic_details_labels_header_h2 = BaseElement()
        self._epic_details_label_input = BaseElement()
        self._epic_details_edit_labels_button = BaseElement()
        self._epic_details_executor_name_span = BaseElement()
        self._epic_details_select_executor_input = BaseElement()
        self._epic_details_executor_field_div = BaseElement()
        self._epic_details_text_editor_save_button = BaseElement()
        self._epic_details_text_editor_editable_div = BaseElement()
        self._epic_details_text_editor_container_div = BaseElement()
        self._epic_details_status_field_wrapper_span = BaseElement()
        self._epic_details_status_field_wrapper_button = BaseElement()
        self._epic_details_specific_color_div = BaseElement()
        self._epic_details_confirm_description_button = BaseElement()
        self._epic_details_header_textarea = BaseElement()
        self._epic_details_status_span = BaseElement()
        self._epic_details_notifier_div = BaseElement()
        self._epic_details_end_date_button = BaseElement()
        self._epic_details_start_date_button = BaseElement()
        self._epic_details_label_link_a = BaseElement()
        self._epic_details_executor_div = BaseElement()
        self._epic_details_description_p = BaseElement()
        self._epic_details_status_field_span = BaseElement()
        self._epic_details_description_div = BaseElement()
        self.current_project = project

    @property
    def epic_details_color_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_color_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="button div[color]"))

    @property
    def epic_details_container_div(self) -> BaseElement:
        return self.get_base_element(self._epic_details_container_div, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="div[data-test-id='roadmap.common.components.panel.container']")
        ))

    @property
    def epic_details_close_span(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_close_span,
                                           self.epic_details_container_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="span[aria-label='Sluiten']"))

    @property
    def table_content_list_div(self) -> BaseElement:
        return self.get_base_element(self._table_content_list_div, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="div[data-test-id='roadmap.common.components.table.components.list.scroll-area.hidden-scrollbar']")
        ))

    @property
    def create_epic_textarea(self) -> BaseElement:
        return self.get_base_element(self._create_epic_textarea, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="textarea[data-test-id='platform-inline-card-create.ui.form.summary.styled-text-area']")
        ))

    @property
    def epic_details_div(self) -> BaseElement:
        return self.get_base_element(self._epic_details_div, BaseElement(
            driver=self.driver,
            locator=LocatorTuple(by=By.CSS_SELECTOR,
                                 value="div[data-test-id='issue.views.issue-details.issue-layout.issue-layout']")
        ))

    def project_epic_name_p(self, project_epic_name) -> BaseElement:
        return self.get_base_element_child(self._project_name_p,
                                           self.table_content_list_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//p[text() = '" + project_epic_name + "']"))

    def project_epic_container_div(self, project_epic_name) -> BaseElement:
        return self.get_base_element_child(self._project_epic_container_div,
                                           self.table_content_list_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//p[text() = '" + project_epic_name + "']/ancestor::div[@data-test-id='roadmap.common.components.table.components.list-item.base.container']"))

    @property
    def create_epic_button(self) -> BaseElement:
        return self.get_base_element_child(self._create_epic_button,
                                           self.table_content_list_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="button[data-test-id='platform-inline-card-create.ui.trigger.visible.button']"))

    def create_epic_spinner_svg(self, project_epic_name) -> BaseElement:
        return self.get_base_element_child(self._create_epic_spinner_svg,
                                           self.project_epic_container_div(project_epic_name),
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[data-test-id*='spinner'] svg"))

    @property
    def epic_details_header_h1(self) -> BaseElement:
        return self.get_base_element_child(self._project_name_p,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="h1[data-test-id='issue.views.issue-base.foundation.summary.heading']"))

    @property
    def epic_details_description_p(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_description_p,
                                           self.epic_details_description_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="p"))

    @property
    def epic_details_description_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_description_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[data-test-id='issue.views.field.rich-text.description']"))

    @property
    def epic_details_executor_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_executor_div,
                                           self.epic_details_executor_field_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[class*='SingleLineTextInput__ReadView-sc-4hfvq0-0']"))

    @property
    def epic_details_executor_field_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_executor_field_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[data-test-id='issue.views.field.user.assignee']"))

    def epic_details_label_link_a(self, label_text) -> BaseElement:
        return self.get_base_element_child(self._epic_details_label_link_a,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//a[text() = '" + label_text + "']"))

    @property
    def epic_details_start_date_button(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_start_date_button,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//button[@aria-label='Start date bewerken']/parent::div"))

    @property
    def epic_details_end_date_button(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_end_date_button,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//button[@aria-label='Vervaldatum bewerken']/parent::div"))

    @property
    def epic_details_notifier_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_notifier_div,
                                           self.epic_details_notifier_field_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[class*='SingleLineTextInput__ReadView-sc-4hfvq0-0']"))

    @property
    def epic_details_updated_span(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_notifier_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//small //span[text()='Bijgewerkt']"))

    @property
    def epic_details_notifier_field_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_notifier_field_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[data-test-id='issue.views.field.user.reporter']"))

    def epic_details_notifier_name_span(self, project_epic_notifier) -> BaseElement:
        return self.get_base_element_child(self._epic_details_notifier_field_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="span[aria-label='" + project_epic_notifier + "']"))

    @property
    def epic_details_notifier_select_input(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_notifier_select_input,
                                           self.epic_details_notifier_field_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="input[id*='react-select']"))

    @property
    def epic_details_header_textarea(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_header_textarea,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="h1[data-test-id*='issue.views.issue-base.foundation.summary.heading'] textarea"))

    def epic_details_status_span(self, project_epic_status) -> BaseElement:
        return self.get_base_element_child(self._epic_details_status_span,
                                           self.epic_details_status_field_wrapper_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//button //span[text() = '" + project_epic_status + "']"))

    def epic_details_status_field_span(self, project_epic_status) -> BaseElement:
        return self.get_base_element_child(self._epic_details_status_field_span,
                                           self.epic_details_status_field_wrapper_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//span[text() = '" + project_epic_status + "']"))

    @property
    def epic_details_status_field_wrapper_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_status_field_wrapper_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[data-test-id='issue.views.issue-base.foundation.status.status-field-wrapper']"))

    def epic_details_specific_color_div(self, project_epic_color) -> BaseElement:
        return self.get_base_element_child(self._epic_details_specific_color_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[color='" + project_epic_color + "']"))

    def epic_details_executor_name_span(self, project_epic_executor) -> BaseElement:
        return self.get_base_element_child(self._epic_details_executor_name_span,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//div[@data-test-id='issue.views.field.user.assignee'] //span[text()='" + project_epic_executor + "']"))

    @property
    def epic_details_confirm_description_button(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_confirm_description_button,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="button[aria-label='Samenvatting bevestigen']"))

    @property
    def epic_details_status_field_wrapper_button(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_status_field_wrapper_button,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[data-test-id='issue.views.issue-base.foundation.status.status-field-wrapper'] button span"))

    @property
    def epic_details_text_editor_container_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_text_editor_container_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[data-test-id='issue.views.field.rich-text.editor-container']"))

    @property
    def epic_details_text_editor_editable_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_text_editor_editable_div,
                                           self.epic_details_text_editor_container_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="div[contenteditable='true']"))

    @property
    def epic_details_text_editor_save_button(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_text_editor_save_button,
                                           self.epic_details_text_editor_container_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="button[data-testid='comment-save-button']"))

    @property
    def epic_details_select_executor_input(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_select_executor_input,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="input[id='react-select-assignee-input']"))

    @property
    def epic_details_edit_labels_button(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_edit_labels_button,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//button[@aria-label='Labels bewerken']/ancestor::div[contains(@class, 'FieldBaseWrapper-sc-14kmybr-0')]"))

    @property
    def epic_details_label_input(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_label_input,
                                           self.epic_details_edit_labels_button,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="input[id*='react-select']"))

    @property
    def epic_details_labels_header_h2(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_labels_header_h2,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//h2[contains(text(), 'Labels')]"))

    @property
    def epic_details_end_date_input_field_wrapper_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_end_date_input_field_wrapper_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//button[@aria-label='Vervaldatum bewerken']/ancestor::div[contains(@class, 'FieldBaseWrapper-sc-14kmybr-0')]"))

    @property
    def epic_details_start_date_input_field_wrapper_div(self) -> BaseElement:
        return self.get_base_element_child(self._start_date_input_field_wrapper_div,
                                           self.epic_details_div,
                                           LocatorTuple(by=By.XPATH,
                                                        value="//button[@aria-label='Start date bewerken']/ancestor::div[contains(@class, 'FieldBaseWrapper-sc-14kmybr-0')]"))

    @property
    def epic_details_start_date_input_select_input(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_start_date_input_select_input,
                                           self.epic_details_start_date_input_field_wrapper_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="input[id*='react-select']"))

    @property
    def epic_details_end_date_input_select_input(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_end_date_input_select_input,
                                           self.epic_details_end_date_input_field_wrapper_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="input[id*='react-select']"))

    @property
    def epic_details_start_date_table_select_date_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_start_date_input_select_input,
                                           self.epic_details_start_date_input_field_wrapper_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="table[class*='Calendar__CalendarTable-sc-3j1sll-1'] div[class='Date__DateDiv-sc-1h7o82l-0 dxKiXv']"))

    @property
    def epic_details_end_date_table_select_date_div(self) -> BaseElement:
        return self.get_base_element_child(self._epic_details_end_date_table_select_date_div,
                                           self.epic_details_end_date_input_field_wrapper_div,
                                           LocatorTuple(by=By.CSS_SELECTOR,
                                                        value="table[class*='Calendar__CalendarTable-sc-3j1sll-1'] div[class='Date__DateDiv-sc-1h7o82l-0 dxKiXv']"))

    def validate_project_epic_details_are_updated(self, project_epic_data: ProjectEpic):

        assert project_epic_data.name in self.epic_details_header_h1.text
        assert project_epic_data.description in self.epic_details_description_p.text

        assert project_epic_data.executor in self.epic_details_executor_div.text

        for label_text in project_epic_data.labels:
            assert label_text in self.epic_details_label_link_a(label_text).text

        assert project_epic_data.start_date.strftime('%Y/%m/%d') in self.epic_details_start_date_button.text

        assert project_epic_data.end_date.strftime('%Y/%m/%d') in self.epic_details_end_date_button.text

        assert project_epic_data.notifier in self.epic_details_notifier_div.text

        assert 'Bijgewerkt' in self.epic_details_updated_span.text

        return project_epic_data.status in self.epic_details_status_span(project_epic_data.status).text

    def click_on_project_epic_name(self, project_epic_name):
        if self.wait_until_base_element_disappears(self.create_epic_spinner_svg(project_epic_name)):
            self.hover_and_click(self.project_epic_name_p(project_epic_name).refresh_and_get_web_element())

    def project_epic_name_exists_in_table(self, project_epic_name):
        if self.project_epic_name_p(project_epic_name).web_element:
            return True

    def create_new_project_epic(self):
        self.create_epic_textarea.send_keys(Keys.ENTER)

    def add_new_project_epic(self, project_epic_data: ProjectEpic):
        if self.create_epic_button.web_element:
            self.create_epic_button.click()
        else:
            self.create_epic_textarea.click()

        self.create_epic_textarea.send_keys(project_epic_data.name + Keys.ENTER)

    def validate_project_epic_exists_on_roadmap(self, project_epic_data: ProjectEpic):
        return project_epic_data.name in self.project_epic_name_p(project_epic_data.name).text

    def change_project_epic_details(self, project_epic_data: ProjectEpic):

        self.epic_details_header_h1.click()

        project_epic_name_textarea = self.epic_details_header_textarea
        # project_epic_name_textarea.clear()

        InputHelper.clear_input_value_with_delete_key(self.epic_details_header_textarea, Keys.DELETE)
        # while project_epic_name_textarea.get_attribute('value'):
        #     project_epic_name_textarea.send_keys(Keys.DELETE)
        project_epic_name_textarea.send_keys(project_epic_data.name)

        self.epic_details_confirm_description_button.click()

        # self.click_with_javascript(self.epic_details_color_div.wait_and_get_web_element())
        self.epic_details_color_div.click()
        self.epic_details_specific_color_div(project_epic_data.color).click()

        self.epic_details_status_field_wrapper_button.click()

        status_button = self.epic_details_status_field_wrapper_button.web_element
        self.hover(status_button)
        self.epic_details_status_field_span(project_epic_data.status).click()

        self.epic_details_description_div.click()

        self.epic_details_text_editor_editable_div.send_keys(
            project_epic_data.description)

        self.epic_details_text_editor_save_button.click()

        self.epic_details_executor_field_div.click()

        self.epic_details_select_executor_input.send_keys(project_epic_data.executor)

        self.epic_details_executor_name_span(project_epic_data.executor).click()

        self.epic_details_edit_labels_button.click()

        label_input = self.epic_details_label_input

        for label_text in project_epic_data.labels:
            label_input.send_keys(label_text + Keys.ENTER)

        self.epic_details_labels_header_h2.click()

        self.epic_details_start_date_input_field_wrapper_div.click()
        self.epic_details_start_date_input_select_input.send_keys(project_epic_data.start_date.strftime("%Y/%m/%d"))

        self.epic_details_start_date_table_select_date_div.click()

        self.epic_details_end_date_input_field_wrapper_div.click()
        self.epic_details_end_date_input_select_input.send_keys(project_epic_data.end_date.strftime("%Y/%m/%d"))

        self.epic_details_end_date_table_select_date_div.click()

        self.epic_details_notifier_field_div.click()
        self.epic_details_notifier_select_input.send_keys(project_epic_data.notifier)

        self.epic_details_notifier_name_span(project_epic_data.notifier).click()

    def close_project_epic_details(self):
        self.epic_details_close_span.click()
